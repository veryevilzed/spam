#!/usr/bin/env python


# Параметры рассылки
MESSAGE='Привет всем' # Сообщеине
START_HOUR = 9        # Час начала рассылки
END_HOUR =   20       # Час окончания рассылки
DELAY = 0.1           # Задержка между сообщениями (в сек.)

# Настройки Kannel
FROM = "pogoda" # С какого номера рассылать
SMSC = "lbssendc" # Имя smsc
USERNAME = "lbssend" # Имя пользователя соединения каннел
PASSWORD = "123" # Пароль соединения каннел
KANNEL_URI = "http://192.168.0.171:13013/cgi-bin/sendsms" # Адрес каннел



# Не трогать
DATABASE = "./work.db"
HOURS_ARRAY = range(START_HOUR, END_HOUR + 1)


import sqlite3
import os
import requests
import time, datetime

def in_hour():
    return datetime.datetime.now().hour in HOURS_ARRAY

def get_msisdn():
    res = ""
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()
    c.execute("SELECT msisdn FROM m LIMIT 1;")
    res = c.fetchone()
    c.execute("DELETE FROM m WHERE msisdn='%s'" % res)
    conn.commit()
    conn.close()
    return res


if not os.path.exists("./work.db"):
    print "Error: work.db not exist!"
    os.exit(1)


msisdn = get_msisdn()

text = unicode(MESSAGE.replace("\n","").replace("\x00","").replace("\x01",""),"utf-8").encode('utf-16-be')

while msisdn:
    if in_hour():
        print "Send %s..." % msisdn

        playload = {
            "username": "",
            "password": "",
            "to": msisdn,
            "from": FROM,
            "text": ,
            "coding": 2,
        }

        r = requests.get(KANNEL_URI, params=payload)
        r.status_code
        print "[%s] %s; (%s) %s" % (datetime.datetime.now().strftime("%Y-%m-%s %H:%M:%S")   msisdn, r.status_code, r.text)
        time.sleep(DELAY)
        msisdn = get_msisdn()
    else:
        time.sleep(60)


print "Done!",
