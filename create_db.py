#!/usr/bin/env python

import os
import os.path
import shutil
import sqlite3

if not os.path.exists("./work.db"):
    print "Copy database ..."
    shutil.copyfile("./msisdn.db", "./work.db")

    conn = sqlite3.connect("./work.db")
    c = conn.cursor()
    print "Remove ignored ..."
    for line in file("./black.csv", "r"):
        if line[0:6] == "msisdn":
            continue
        msisdn = line[1: len("79204114299")]
        c.execute("DELETE FROM m WHERE msisdn='%s'" % msisdn)
    conn.commit()
    conn.close()

    print "Done"
else:
    print "Database file already exist!"
